FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# renovate: datasource=repology depName=alpine_3_21/openssh depType=dependencies versioning=loose
ARG OPENSSH_VERSION="9.9_p2-r0"

RUN apk add --no-cache \
        bash~=5 \
        openssh="${OPENSSH_VERSION}" \
        openssh-sftp-server="${OPENSSH_VERSION}" \
        supervisor~=4 \
        iproute2~=6 \
        inotify-tools~=4 \
        procps~=4 \
        moreutils~=0 \
        run-parts~=4 \
        gettext~=0 \
        shadow~=4 \
        diffutils~=3 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3

COPY files/ /

ENV APP_SSH_HOSTKEY_DIR="/opt/ssh-hostkeys" \
    APP_SSH_UMASK="0027" \
    APP_USERS_KEYS_DIR="/opt/sftp-keys" \
    APP_USERS_PASSWD_FILE="/opt/sftp-users/passwd" \
    APP_TS_FORMAT="%F %.T%z" \
    APP_UID_MIN="10000" \
    APP_UID_MAX="19999" \
    APP_GID_MIN="10000" \
    APP_GID_MAX="19999"

EXPOSE 22

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"

CMD ["/usr/local/sbin/docker-entrypoint.sh"]
