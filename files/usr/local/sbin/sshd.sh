#!/usr/bin/env bash

set -Eeuo pipefail

trap 'kill -USR1 -- "$CHILD_PID"; wait "${CHILD_PID}"' SIGUSR1
trap 'kill -USR2 -- "$CHILD_PID"; wait "${CHILD_PID}"' SIGUSR2
trap 'kill -HUP -- "$CHILD_PID"; wait "${CHILD_PID}"' SIGHUP
trap 'kill -QUIT -- "$CHILD_PID"; wait "${CHILD_PID}"; exit 0' SIGQUIT
trap 'kill -INT -- "$CHILD_PID"; wait "${CHILD_PID}"; exit 0' SIGINT
trap 'kill -TERM -- "$CHILD_PID"; wait "${CHILD_PID}"; exit 0' SIGTERM

/usr/sbin/sshd -e -D > >(ts "$APP_TS_FORMAT") 2> >(ts "$APP_TS_FORMAT" >&2) &

CHILD_PID="$!"
wait "${CHILD_PID}"
