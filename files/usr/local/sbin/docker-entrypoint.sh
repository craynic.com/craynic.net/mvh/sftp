#!/usr/bin/env bash

set -Eeuo pipefail

# run the parts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d/" > >(ts "$APP_TS_FORMAT") 2> >(ts "$APP_TS_FORMAT" >&2)

# run supervisord
/usr/bin/supervisord -c /etc/supervisord.conf -n
