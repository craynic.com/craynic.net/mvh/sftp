#!/usr/bin/env bash

set -Eeuo pipefail

getCurrentUsers() {
  getent passwd | awk -F: "(\$3 >= $APP_UID_MIN && \$3 <= $APP_UID_MAX)" | sort
}

getCurrentGroups() {
  getent group | awk -F: "(\$3 >= $APP_GID_MIN && \$3 <= $APP_GID_MAX)" | sort
}

PASSWD_BEFORE="$(getCurrentUsers)"

# remove all users
getCurrentUsers | cut -d: -f1 | while read -r USER_NAME; do
  userdel -- "$USER_NAME"
done

# remove all groups if any
getCurrentGroups | cut -d: -f1 | while read -r GROUP_NAME; do
  groupdel -- "$GROUP_NAME"
done

# add all users again
CONFIG_USERS="$( (grep -v "^\(#\|$\)" || true) < "$APP_USERS_PASSWD_FILE" | sort)"

echo "$CONFIG_USERS" | (grep -v "^$" || true) | while read -r CONFIG_USER_LINE; do
  readarray -td":" CONFIG_USER_DATA <<< "$CONFIG_USER_LINE"

  CONFIG_USERNAME="${CONFIG_USER_DATA[0]}"
  CONFIG_UID="${CONFIG_USER_DATA[2]}"
  CONFIG_GID="${CONFIG_USER_DATA[3]}"

  groupadd -g "$CONFIG_GID" -- "$CONFIG_USERNAME"
  useradd -M \
    -u "$CONFIG_UID" \
    -g "$CONFIG_GID" \
    -c "${CONFIG_USER_DATA[4]}" \
    -d "${CONFIG_USER_DATA[5]}" \
    -s /bin/false \
    -- \
    "$CONFIG_USERNAME"

  # set the password
  echo "$CONFIG_USERNAME:${CONFIG_USER_DATA[1]}" | chpasswd -e
done

diff <(echo "$PASSWD_BEFORE") <(getCurrentUsers) || true

# find users of all SSHD processes that are numeric (=don't have user)
ps aux | grep -i sshd | awk -F" " "(\$1 >= 10000 && \$1 <= 19999) { print \$1 } " | sort | uniq | \
  while read -r UID_TO_KICK; do
    pkill -u "$UID_TO_KICK"
  done
