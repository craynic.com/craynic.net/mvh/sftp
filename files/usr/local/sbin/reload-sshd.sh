#!/usr/bin/env bash

set -Eeuo pipefail

# reload SSHD
supervisorctl status sshd >/dev/null && supervisorctl signal HUP sshd > >(ts "$APP_TS_FORMAT")
