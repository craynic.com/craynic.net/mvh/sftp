#!/usr/bin/env bash

set -Eeuo pipefail

trap 'pkill -USR1 -P "$$"; wait "${CHILD_PID}"' SIGUSR1
trap 'pkill -USR2 -P "$$"; wait "${CHILD_PID}"' SIGUSR2
trap 'pkill -QUIT -P "$$"; wait "${CHILD_PID}"; exit 0' SIGQUIT
trap 'pkill -INT -P "$$"; wait "${CHILD_PID}"; exit 0' SIGINT
trap 'pkill -TERM -P "$$"; wait "${CHILD_PID}"; exit 0' SIGTERM

inotifywait-dir.sh "$APP_USERS_PASSWD_FILE" | while read -r LINE; do
  echo "$LINE"

  /usr/local/sbin/update-users.sh > >(ts "$APP_TS_FORMAT") 2> >(ts "$APP_TS_FORMAT" >&2)

  /usr/local/sbin/reload-sshd.sh
done &

CHILD_PID="$!"
wait "${CHILD_PID}"
