#!/usr/bin/env bash

set -Eeuo pipefail

(
  echo ""
  echo "UID_MIN $APP_UID_MIN"
  echo "UID_MAX $APP_UID_MAX"
  echo "GID_MIN $APP_GID_MIN"
  echo "GID_MAX $APP_GID_MAX"
) >> /etc/login.defs

