#!/usr/bin/env bash

set -Eeuo pipefail

# this is meant for local development environment
# in production, you want to mount a secret to the path $APP_SSH_HOSTKEY_DIR
# - secrets are typically read-only, so you'll have to pre-generate the secrets before using this container

if [ ! -f "$APP_SSH_HOSTKEY_DIR/ssh_host_rsa_key" ]; then
    ssh-keygen -t rsa -b 4096 -f "$APP_SSH_HOSTKEY_DIR/ssh_host_rsa_key" -N ''
    chmod 0600 "$APP_SSH_HOSTKEY_DIR/ssh_host_rsa_key"
fi

if [ ! -f "$APP_SSH_HOSTKEY_DIR/ssh_host_ecdsa_key" ]; then
    ssh-keygen -t ecdsa -b 521 -f "$APP_SSH_HOSTKEY_DIR/ssh_host_ecdsa_key" -N ''
    chmod 0600 "$APP_SSH_HOSTKEY_DIR/ssh_host_ecdsa_key"
fi

if [ ! -f "$APP_SSH_HOSTKEY_DIR/ssh_host_ed25519_key" ]; then
    ssh-keygen -t ed25519 -f "$APP_SSH_HOSTKEY_DIR/ssh_host_ed25519_key" -N ''
    chmod 0600 "$APP_SSH_HOSTKEY_DIR/ssh_host_ed25519_key"
fi
