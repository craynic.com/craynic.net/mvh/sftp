#!/usr/bin/env bash

set -Eeuo pipefail

WAIT_BETWEEN_ATTEMPTS="1"
WAIT_TIMEOUT="60"

until [[ -f "$APP_USERS_PASSWD_FILE" ]]; do
  if [[ "$SECONDS" -ge "$WAIT_TIMEOUT" ]]; then
    echo "I have waited $SECONDS seconds for the \"$APP_USERS_PASSWD_FILE\"" \
      "to appear and don't want to wait anymore, exiting..."
    exit 1
  fi

  echo "Waiting for \"$APP_USERS_PASSWD_FILE\" file to appear..."
  sleep "$WAIT_BETWEEN_ATTEMPTS"
done

echo "File \"$APP_USERS_PASSWD_FILE\" appeared, proceeding..."
