# SFTP server for Virtual Mass Hosting

## Usage

* Mount your SSH **public** keys as `/opt/sftp-keys/%u/authorized_keys` (default location) where `%u`
  will be replaced by the username. The location can be changed with environmental variable `APP_USERS_KEYS_DIR`.

* Define your users in the file `/opt/sftp-users/passwd` (default location) with the "/etc/passwd"
structure. 

  The structure of the file is: 
  ```
  user:password:uid:gid:name:homedir:shell
  ```  
  * The shell part is ignored
  * The password must be encrypted; it might be omitted (empty)

  The location of the file can be changed with environmental variable `APP_USERS_PASSWD_FILE`.
